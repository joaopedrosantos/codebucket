CodeBucket
==================

Description
------------------
CodeBucket is the best way to browse and maintain your Bitbucket repositories on any iPhone, iPod Touch, and iPad device! Keep an eye on your projects with the ability to view everything from followers to the individual file diffs in the latest change set. CodeBucket brings Bitbucket to your finger tips in a sleek and efficient design.

Source Code
------------------
CodeBucket's source code [can be found here](https://github.com/thedillonb/CodeBucket).

Screenshots
------------------

![Repositories.png](https://bitbucket.org/repo/BKgjaX/images/822388267-Repositories.png)
![RepositoryView.png](https://bitbucket.org/repo/BKgjaX/images/2085838588-RepositoryView.png)
![Issues.png](https://bitbucket.org/repo/BKgjaX/images/2625769783-Issues.png)
![Slideout.png](https://bitbucket.org/repo/BKgjaX/images/3172862486-Slideout.png)
![ChangeSets.png](https://bitbucket.org/repo/BKgjaX/images/3207867575-ChangeSets.png)

Copyright
-----------------
Copyright 2012 Dillon Buchanan